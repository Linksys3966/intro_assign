
package com.tw;

public class Nine {

    public static void main(String[] args) throws Exception {
        generate(30);
    }

    static void generate(int n) {
        for (int i = 2; i < n; i++) {
            if ((n % i) == 0 && prime(i)) {
                System.out.println(i);
            }
        }
    }

    private static boolean prime(int n) {
        for (int i = 2; i < n; i++) {
            if (n % i == 0) {
                return false;
            }
        }
        return true;
    }
}
