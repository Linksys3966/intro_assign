package com.tw;
public class Diamonds {
    public static void main(String[] args) {
        int lines = 5;
        int width = lines;
        for (int line = 0; line <= (lines * 2)+1; line++) {
            if(line==(width-1))
            {
                line=line+2;
            }
            for (int spaces = 0; spaces < Math.abs(line - lines); spaces++) {
                System.out.print(" ");
            }
            for (int marks = 0; marks < 2 * (width - (Math.abs(line - lines))) + 1; marks++) {
                System.out.print("*");

            }
            System.out.println();
        }
        System.out.println();

    }
}
